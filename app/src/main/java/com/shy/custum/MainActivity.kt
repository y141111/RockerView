package com.shy.custum

import android.os.Bundle
import android.util.Log
import android.widget.SeekBar
import android.widget.SeekBar.OnSeekBarChangeListener
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import com.shy.rockerview.MyRockerView
import com.shy.rockerview.SeekBarVertical

class MainActivity : AppCompatActivity() {
    companion object {
        const val TAG = "MainActivity"
    }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        //方向有改变时回调
        val mRockerViewXY = findViewById<MyRockerView>(R.id.rockerXY_View)  // 8方向
        val mRockerViewZ = findViewById<MyRockerView>(R.id.rockerZ_View)    // 4方向
        val mSeekBarVertical = findViewById<SeekBarVertical>(R.id.seekbarVertical_View)    // seekbar


        val directionXY_Text = findViewById<TextView>(R.id.directionXY_Text)    // 当前方向
        val angleXY_Text = findViewById<TextView>(R.id.angleXY_Text)    // 当前角度
        val levelXY_Text = findViewById<TextView>(R.id.levelXY_Text)    // 当前偏移级别
        val directionZ_Text = findViewById<TextView>(R.id.directionZ_Text) // 当前方向
        val angleZ_Text = findViewById<TextView>(R.id.angleZ_Text) // 当前角度
        val levelZ_Text = findViewById<TextView>(R.id.levelZ_Text) // 当前偏移级别

        // xy轴
        // 1方向
        mRockerViewXY.setOnShakeListener(MyRockerView.DirectionMode.DIRECTION_8, object : MyRockerView.OnShakeListener {
            override fun onStart() {}
            override fun direction(direction: MyRockerView.Direction) {
                var directionXY = ""
                if (direction == MyRockerView.Direction.DIRECTION_CENTER) {
                    directionXY = "当前方向：中心"
                } else if (direction == MyRockerView.Direction.DIRECTION_DOWN) {
                    directionXY = "当前方向：下"
                } else if (direction == MyRockerView.Direction.DIRECTION_LEFT) {
                    directionXY = "当前方向：左"
                } else if (direction == MyRockerView.Direction.DIRECTION_UP) {
                    directionXY = "当前方向：上"
                } else if (direction == MyRockerView.Direction.DIRECTION_RIGHT) {
                    directionXY = "当前方向：右"
                } else if (direction == MyRockerView.Direction.DIRECTION_DOWN_LEFT) {
                    directionXY = "当前方向：左下"
                } else if (direction == MyRockerView.Direction.DIRECTION_DOWN_RIGHT) {
                    directionXY = "当前方向：右下"
                } else if (direction == MyRockerView.Direction.DIRECTION_UP_LEFT) {
                    directionXY = "当前方向：左上"
                } else if (direction == MyRockerView.Direction.DIRECTION_UP_RIGHT) {
                    directionXY = "当前方向：右上"
                }
                Log.e(TAG, "XY轴$directionXY")
                Log.e(TAG, "-----------------------------------------------")
                directionXY_Text.text = directionXY
            }
            override fun onFinish() {}
        })
        // 1角度
        mRockerViewXY.setOnAngleChangeListener(object : MyRockerView.OnAngleChangeListener {
            override fun onStart() {}
            override fun angle(angle: Double) {
                var angleXY = ""
                angleXY = "当前角度：$angle"
                Log.e(TAG, "XY轴$angleXY")
                angleXY_Text.setText(angleXY)
            }

            override fun onFinish() {}
        })
        // 1级别
        mRockerViewXY.setOnDistanceLevelListener { level ->
            var levelXY = ""
            levelXY = "当前距离级别：$level"
            Log.e(TAG, "XY轴$levelXY")
            levelXY_Text.setText(levelXY)
        }


        // 2方向
        mRockerViewZ.setOnShakeListener(MyRockerView.DirectionMode.DIRECTION_4_ROTATE_45, object : MyRockerView.OnShakeListener {
            override fun onStart() {}
            override fun direction(direction: MyRockerView.Direction) {
                var directionXY = ""
                when (direction) {
                    MyRockerView.Direction.DIRECTION_CENTER -> {
                        directionXY = "当前方向：中心"
                    }
                    MyRockerView.Direction.DIRECTION_DOWN -> {
                        directionXY = "当前方向：下"
                    }
                    MyRockerView.Direction.DIRECTION_LEFT -> {
                        directionXY = "当前方向：左"
                    }
                    MyRockerView.Direction.DIRECTION_UP -> {
                        directionXY = "当前方向：上"
                    }
                    MyRockerView.Direction.DIRECTION_RIGHT -> {
                        directionXY = "当前方向：右"
                    }
                    else -> {}
                }
                Log.e(TAG, "XY轴$directionXY")
                Log.e(TAG, "-----------------------------------------------")
                directionZ_Text.text = directionXY
            }
            override fun onFinish() {}
        })

        // 2角度
        mRockerViewZ.setOnAngleChangeListener(object : MyRockerView.OnAngleChangeListener {
            override fun onStart() {}
            override fun angle(angle: Double) {
                var angleXY = ""
                angleXY = "当前角度：$angle"
                Log.e(TAG, "XY轴$angleXY")
                angleZ_Text.setText(angleXY)
            }

            override fun onFinish() {}
        })
        // 2级别
        mRockerViewZ.setOnDistanceLevelListener { level ->
            var levelXY = ""
            levelXY = "当前距离级别：$level"
            Log.e(TAG, "XY轴$levelXY")
            levelZ_Text.setText(levelXY)
        }
        mSeekBarVertical.setOnSeekBarChangeListener(object :OnSeekBarChangeListener{
            override fun onProgressChanged(seekBar: SeekBar?, progress: Int, fromUser: Boolean) {
                println(progress)
            }

            override fun onStartTrackingTouch(seekBar: SeekBar?) {
                TODO("Not yet implemented")
            }

            override fun onStopTrackingTouch(seekBar: SeekBar?) {
                TODO("Not yet implemented")
            }
        })
    }
}